---
author: Vibhoothi
date: '2016-09-11'
slug: rhinoshield-crash-guard-for-oneplus-two-review
title: Rhinoshield Crash Guard For Oneplus Two Review
project: 'Reviews'
---

Hello Everybody today lets take a look at Evolutive Lab's Rhinoshield Crash Guard bumper for Oneplus Two .The Design is neat and clean with little bit tough at moving alert slider rather than that its cool addition to your lovely device. Although the bumper adds a few mm to your phone yet its all the worth to your money


![image500c](https://vibhoothiiaanand.files.wordpress.com/2016/09/20160829_125100.jpg)


The design its look very sleek and stylish. The two tone color construction makes it more attractive. The hardened buttons make very easy and stiff to press buttons and increases the grip of the phone. The Headphone jack cutouts, Speaker cutouts, Charger cutouts and secondary noise cancellation microphone cutouts all are perfectly built.

![image500c](https://vibhoothiiaanand.files.wordpress.com/2016/09/20160829_124309.jpg)


By Providing Raised thickness to the case which is fair enough for about 12feet drop to concrete. I have done a simple drop test which proves it is the best accessories you can spend on your device, sorry guys I am not ready to put this device to massive 12feet drop, but I have done 6-7feet drop test, you can check the quality of product link to the drop test is done you can check it at least.

![image500c](https://vibhoothiiaanand.files.wordpress.com/2016/09/20160829_125004.jpg)

![image500c](https://vibhoothiiaanand.files.wordpress.com/2016/09/20160829_124606.jpg)

It is impossible to have a case that offers total protection of your smartphone while still allowing you to continue to use it with little fuss. There will always be some combination of speed, force, and angle, that will cause damage. The focus of any good protective case is to make the survival envelope as large as possible, and to cover as many real-world examples of damage as possible.

![image500c](https://vibhoothiiaanand.files.wordpress.com/2016/09/20160829_124803.jpg)

I haven't used their screen protector, but I used ordinary tempered glass brought in local shop which I think will do the job .The Crash Guard delivers what it promises  a bumper case for your smartphone that offers significant protection in regular use. It might not make the phone indestructible, but it will give a heavy protection to your device that I can give you a guarantee. 

![image500c](https://vibhoothiiaanand.files.wordpress.com/2016/09/20160829_124800.jpg)

The pricing of the product is $25 dollars I paid through international credit card and shipped to India the shipping was done through the India Post International which took around 9 days to reach my doorstep. I have also done an unboxing video you can check it out it is also there down in the description.


## Reference Links

Drop Test: [YouTube](https://www.youtube.com/watch?v=jmKxB-y6Y44)

Unboxing: [YouTube](https://www.youtube.com/watch?v=Q-Kzfrm1YPQ)


_This blog was publihsed in [WordPress](https://vibhoothiiaanand.wordpress.com/2016/09/11/rhinoshield-crash-guard-for-oneplus-two-review/) on September 11, 2016_

