---
author: Vibhoothi
date: '2016-04-09'
slug: audio-technica-sonic-fuel-ath-ax1is-over-the-ear-headphone-review
title: "Full Review: Audio Technica Sonic Fuel  ATH-AX1iS Over the Ear Headphones"
project: Reviews
---

Hello Guys Last week i brought my first Audio Technica first headpone belive me it was worth spending it .!

![image500c](https://vibhoothiiaanand.files.wordpress.com/2016/04/img_1139.jpg)

I bought it from [Amazon.in](http://amzn.to/1qCQZyW), it took 2days to get the product to my doorstep the delivery was quite fast the packing is awesome from both Amazon and Audio Technica.

[Unboxing Video](https://www.youtube.com/watch?v=KwZaV2KlvJ8) is also there, sorry for low clarity and the video is taken by me and unboxed my me so you can guess what it will be :) sorry for that next time I will be better :D.There will be few questions in your mind.Is it durable? Is it building quality OK? How is the Bass?How is it comforting? And Many more. Anyways, in simple words: 'An Awesome Headset under 2000 Bucks ($30)'.



## **SPECIFICATIONS**

  * _Type_– Dynamic Closed-back
	
  * _Frequency Response_– 15 to 22,000 HZ
	
  * _Diameter of Driver_ – 36 mm
	
  * _Input Power (Maximum)_– 500 mw
	
  * _Sensitivity_ – 100 dB per mW
	
  * _Cord_ – 1.2 m or 3.9 ft
	
  * _Impedance_ – 40 ohms
	
  * _Connector_ – 3.5 mm or 1/8 inches, L-type, gold-plated, mini stereo

## **BUILD AND QUALITY**

Let's not forget that it is an International Quality Checked and approved product so that it will have awesome quality under the hood, anyways the matte finish headset, more than a meter  wire, Soft and smooth Ear pads, Rigid plastic body, cool universal MIC, Volume Rocker well Placed with nice design too.

The headphone comes with well-cushioned ear pads that make it very comfortable for extended use. I have  used  it for around 5 hours at a time and haven’t had pain.

![image500c](https://vibhoothiiaanand.files.wordpress.com/2016/04/img_1142.jpg)

![image500c](https://vibhoothiiaanand.files.wordpress.com/2016/04/img_1143.jpg)

![image500c](https://vibhoothiiaanand.files.wordpress.com/2016/04/img_1145.jpg)

![image500c](https://vibhoothiiaanand.files.wordpress.com/2016/04/img_1146.jpg)



## **PERFORMANCE**

The Crisp clarity of headphone is one the best features of this, mainly because of this is making it one of a kind. The bass is not boosted, but you can hear every background voice inside the song. The Noise Cancellation is also very good.

![image500c](https://vibhoothiiaanand.files.wordpress.com/2016/04/img_1160.jpg)

## **PRICE**

Priced at less than 2,000 its worth spending it

You can buy from either [Amazon.in](http://amzn.to/1qCQZyW) or from [Flipkart.com](http://fkrt.it/mdMJ~NNNNN)



_This blog was originally written in [Wordpress](https://vibhoothiiaanand.wordpress.com/2016/04/09/audio-technica-sonic-fuel-ath-ax1is-over-the-ear-headphone-review/) on 9th April 2016_