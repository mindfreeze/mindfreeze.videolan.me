---
author: Vibhoothi
date: '2015-12-10'
slug: notable-trends-in-smartphone-technology-of-2015
title: Notable trends in Smartphone technology of 2015
Project: 'Other'
---

Hi,

I would like to say, few trends which are going to be mainstream in upcoming months and years from 2015.


 ## 1. 64-bit computing going mainstream 

This year, 64-bit computing became a standard affair, even in mid-tier and budget segments. Right from first-time brands to the Sonys and Apples of the industry, every smartphone maker went for powering devices with 64-bit chipsets. Take for instance the Yu Yunique, which is powered by a 64-bit Snapdragon 410 processor, for a price of Rs. 5,999. Budget segments are no longer pushovers, and 64-bit processors made a crucial case for these devices

	
## 2. 4GB and LPDDR4 RAM in phones.

The advent of 64-bit computing meant that chipsets could handle 4GB of RAM, and that’s what we saw this year. OnePlus gave their second phone, the OnePlus 2, 4GB of LPDDR4 RAM, while Asus got there first with the top-of-the-line Zenfone 2, but with DDR3 RAM. The OnePlus 2 became the first phone to offer 4GB LPDDR4 RAM, shortly followed by Samsung, with the Galaxy Note 5

## 3. 20+ MP Cameras


The megapixel count has never been a yardstick by which the camera quality of any imaging device can be judged. The same applies for smartphone cameras, but it never hurts to have more pixels packed into an image frame. This year, Sony came out with a new Exmor RS Mobile image sensor, which had a maximum capability to render 25-megapixel images. While processing it for the Sony Xperia Z5 meant that it could go up to a highest possible resolution of 23.7 megapixels (4:3) by letterboxing, and 20 megapixels (16:9) by pillarboxing, the new mobile sensor is one of the very first semi-professional image sensors – a mobile version of the same one used in the Sony RX100 Mark IV.

## 4. Dual-camera setups, and the love of selfies!

2015 saw many smartphones taking a second look at dual-camera setups, but to be very honest, there hasn’t been much improvement. We have a promising entrant with the QiKU Q Terra which uses a primary Sony IMX278 sensor to capture colour details and a secondary IMX214 sensor to capture a black and white image of the same, thereby providing finer contrast and depth to images.On the other hand, 2015 was a great year for selfies. There were many phones which offered 13MP cameras at the front, most notably the Asus Zenfone Selfie, which packed in a Toshiba 13-megapixel sensor along with a wide angle lens, and more controls for enhanced ‘selfie’ photography. Lenovo also launched the Vibe S1, which had a dual-camera front-facing setup.



## 5 . Fingerprint sensors are now available on budget phones

Prior to this year, fingerprint sensors were privy to flagship devices, but recently, we are seeing mid-range and budget segment phone offering this security feature.

## 6. High pixel density displays

2015 was a year for 2K resolution displays on phones and they have mostly been great, but Sony went a step further and released the first phone to offer 4K resolution on a smartphone. Such high resolutions on small size displays mean that they are densely packed with pixels, sometimes more than what the human eye can register. For example, the 4K display on the Sony Xperia Z5 Premium features 4K resolution display on a 5.5-inch panel, resulting in an eye-watering 806ppi pixel density.

## 7. Slimmer bezels, larger screens

Phones with 5-inch or smaller displays are becoming a rare sight nowadays, and the average display size in smartphones continues to grow. Surveys have pointed at India’s love for 5-inch-odd smartphone displays – not surprising, seeing that even the cheapest devices now feature 5-inch screens, and the flagships feature 5.5- to 5.7-inch screens.

Also doing the rounds with almost every other smartphone leaks and renders is bezel-less displays. The demand for larger screens is higher, but so are wishes for compact bodies which serve the basic purpose of a phone – fit into the pocket. In such times, we saw screen-to-body ratios increase, leading to slimmer bezels and attempts of keeping devices ergonomic.

## 8. The ‘hot’ problems

2015 was also the year which saw a massive outrage on smartphones heating up. The Qualcomm Snapdragon 810 was a very powerful device, but suffered massively because of its heating issues, making the smartphone-loving world going frenzy with concerns of phones heating up. OEMs started taking this issue increasingly seriously, making it a point to state that their devices “do not heat up”. OnePlus went for additional thermal insulation, Sony fit its flagship with twin-pipe heat dispersers, and Microsoft went ahead and fit a liquid-cooling unit in the recently-launched Windows flagship.

The issue still persists, and ‘performance in phones’ now has a companion parameter – ‘heating in phones’.

## 9. Higher battery lives

Phones are getting more powerful every year, and the current battery technology is unable to keep up with it. Not waiting for the next round of innovation to become mainstream, OEMs are offering phones with larger battery packs along with a decent overall package. Currently, the Oukitel K10000 by far has the biggest battery pack that we have heard of in a smartphone – a mammoth 10000mAh.

## 10. USB Type-C gaining popularity

Google confirmed that USB Type-C will be considered as the new standard moving forward, and many other OEMs are following in its footsteps. The USB Type-C makes way for a reversible port, and if used with USB 3.0 or above, offers better data transfer speeds as well.

## 11. Glass and metal bodies

Many OEMs have moved away from plastic as a build material, and joined the glass-back bandwagon. Samsung, Sony, and even comparatively smaller brands like OnePlus went for beauty over functionality. We also saw more phones offering water resistance, and also, metal builds in cheaper devices.

## 12. Mid-range ‘flagships’

2015 saw the fight of flagship smartphones shift from the high-end devices, to the mid-range ones, giving rise to the term, ‘mid-range flagships’. Companies like Honor, Asus and OnePlus brought flagship specifications to the mid-range and upper-mid-range segments, thereby offering more value for the average buyer’s money.

## 13. Force Touch, a.k.a. 3D Touch

Apple introduced a new technology called 3D Touch for its iPhone 6s and 6s Plus. The new feature gives you an overview of any app if you press the icon gently, and more details once you press it harder, thereby distinguishing different levels of touch pressure input on a screen. This feature is also there in the new MacBook, and the Apple Watch. Other companies are also expected to follow soon, with Huawei bringing in a similar technology, terming it Force Touch.

## 14. Continuum: Converting your phone into a PC

Microsoft added another neat feature in its latest Windows phones with the launch of Continuum in Windows 10. This feature lets you connect a Windows phone to a larger screen through a Microsoft dock, thereby converting your mobile device into a functioning desktop experience. Not the most refined yet, but a definite step forward.

References from Digit, Verge, Android Police.
