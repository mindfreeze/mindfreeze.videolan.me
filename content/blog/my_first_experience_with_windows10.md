---
title: My First Experience With Windows 10
author: Vibhoothi
date: '2015-08-02'
slug: my-first-expirience-with-windows-10
project: others
---


After about a few hours with the latest operating system, here's what I noticed first about Windows 10

We have known almost everything about what’s going to be new with Windows 10 for some time now, and here it is for us to experience. We’ve spent a couple of hours fidgeting around with it, and here’s our thought after some time of usage:


## **Speed**

Windows 10 has been really, really fast till now. Booting to the desktop takes 42 seconds on average, including entering a 10-character password. Running apps  in the background, and there is no hint of a stutter. Everything from navigating through folders to launching an application and playing a new movie is so far a breeze, and Windows 10 seems to be the most responsive Windows till date. And last But Not the least the mutiple handing is top notch .


## **Layout**

The Main advantage is that there is no need to drag to cursor to the end of the screen and wait for a second or to go to setting or go to turn off system now its all in one start menu so no worry all just like our old buddy windows 7 the people's favorite.
Microsoft has made a number of changes with Windows 10’s layout. The Start menu is back, featuring a look similar to Windows 8’s Start screen. Neatly categorized into a number of sections, the Start menu features live tiles for applications, a most-used listing and action shortcuts. The user account settings are on the top-left corner of the Start menu. The hidden Search and Actions panel that used to float in from the right in Windows 8 has been replaced by a universal Search panel on the taskbar, and a notifications panel that can be accessed from the System Icons tray. Notably, the notifications panel has a layout similar to Windows Mobile. Windows now has a new Task View feature - Mac-esque view that shows all the open and running windows in one screen.

The icons have been updated, and the windows now have narrow, redesigned borders. The Windows logo is also new, and there is added transparency in notifications and connections panels.

## **Edge**
[Microsoft’s new browser](http://www.digit.in/internet/microsoft-edge-debuts-windows-10s-new-web-browser-25941.html) looks neat and clean . Microsoft has really worked on refining layouts and making the entirety of Windows 10 look seamless. Browsing seems fast, and Microsoft has added InPrivate browsing mode - equivalent to Chrome’s Incognito. However, it isn’t as fast and smooth as Microsoft claimed. There were a couple of stutters while switching between tabs, and pages took comparatively more time to load than Chrome. It is, however, a huge upgrade over Internet Explorer, and it is a very noticeable upgrade over Microsoft’s old browser. The transitions between the tabs and also noticeable


## **Other features**
The welcome screen that precedes the sign-in screen while loading Windows gives you an option to like or dislike photos that come in automatic stream, which will lead to Microsoft providing you with a curated stream of photos on the welcome display.

## **Virtual workspaces**

Linux and OSX users will be well versed with the concept of virtual desktops / workspaces. This feature allows users to group related windows together and then switch between these groups of windows just as you can alt-tab between individual windows right now.

Each group of windows gets it’s own virtual screen area allowing you to have different sessions active at a time while focusing on only one.  For instance, you can have a document editor and browser open in one virtual desktop where you are writing a report, while another virtual desktop can have a photo editor and a file manager showing your photo collection, while yet another could house your music player. As you switch between virtual desktops, only the windows that are part of the current virtual desktop are visible and the rest are absent even from alt-tab.

## **A Proper Windows Store**


Windows 8 introduced a new store, but the store was overly restrictive. Firstly, it would only offer the new ModernUI applications which themselves were introduced for the first time with Windows 8. Additionally, these kinds of applications would only be available from the Windows store.

For Windows 10, Microsoft has already released developer tools that allow for the creation of ‘traditional’ Win32 apps — the kind that ran on Windows 7 and below, not the new Modern / Metro apps  — that can be sold on the Windows store.

Worry not though, traditional applications will still be installable and runnable as always. That feature is absolutely essential.

Microsoft will need to do a better job of managing the store than they have done so far though as the Windows store is rife with fraudulent, fake apps.

There is also the new Action Centre, which has a mobile format layout, with all of its settings arranged into a grid. Notifications also appear in a mobile-like view. The Windows Store has been reshuffled and presented in a smoother format. [Cortana](http://www.digit.in/software/windows-10-cortana-to-be-available-on-pcs-25065.html)? Not for India, as of now.

Microsoft seems to have done well with Windows 10. There still are a number of glitches, but with Microsoft having stated rolling updates to Windows 10, we can expect the gaps to be filled. Stay tuned for a further, detailed look into Windows 10.

## **Conclusion**

There is a lot more that Windows 10 has to offer to developers, and hardware enthusiasts. For instance, Microsoft has developed tools to enable developers to easily port Android and iOS apps to Windows. They have also made versions of Windows available to install on devices such as the Raspberry Pi 2 and are working to make it possible to install a Windows 10 ROM on your mobile like you would a CyanogenMod ROM.


For those with more constrained systems with limited storage space, there is yet more good news. Windows 10 will greatly reduce the size of the Windows install by removing the need for a recovery partition, and by compressing system files.

Then of course, there is the whole business of the Hololens, Windows’ foray into augmented reality that has a lot of people excited. There are a ton of things to look forward to in Microsoft’s latest OS, and many of them you can already experience in the pre-release preview versions available today.

## **A Smaller Install Size**

In light of all the other features we have mentioned, this may seem almost trivial, but it is of import, believe us!

Recent versions of Windows have ballooned to unreasonable sizes; a Windows folder of 30GB isn’t considered ridiculous, it’s  expected. With hard drives touching 4TB capacities and getting increasingly cheaper, this wasn’t much of a big deal. However, recent years have seen the rise of ultraportable devices such as tablets that feature much more limited amounts of solid state storage.

Microsoft has introduced a number of measures to reduce the footprint of a Windows install, firstly by introducing new ways of compressing system files that can reduce the size of an install by as much 2.6GB. It has also eliminated the need for a recovery partition on Windows systems, bringing an additional saving of at least 4GB and possibly more.

With Reference To DIGIT TEAM 


_Orginally Posted on [Wordpress](https://vibhoothiiaanand.wordpress.com/2015/08/02/my-first-expirience-on-windows-10/ ) on August 2 2015_ 