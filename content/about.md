---
title: About
author: Vibhoothi
---

[Vibhoothi](https://www.linkedin.com/in/vibhoothi/), is a Research Assistant in Department of Electronic and Electrical Engineering in [Trinity College Dublin](https://tcd.ie), and also a PhD Student working under [Anil Kokaram](https://www.linkedin.com/in/anil-kokaram-8661417/?originalSubdomain=ie) on Optimised Transcoding and compression related research within [AV1 Codec](https://en.wikipedia.org/wiki/AV1).  He is holding a bachelors degree in Computer Science and Engineering from [Amrita University](https://amrita.edu). 

He is also part of [sigmedia.tv](http://sigmedia.tv) group in the university. 

He is also an active member and collaborator of various open-source multimedia initiatives and projects like [Alliance for Open-Media](http://aomedia.org/), [Xiph.org](https://xiph.org) and [VideoLAN.](https://videolan.org) projects.

Email: vibhoothi@tcd.ie, mindfreeze@videolan.org
